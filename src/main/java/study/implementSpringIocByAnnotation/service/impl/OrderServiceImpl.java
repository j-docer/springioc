package study.implementSpringIocByAnnotation.service.impl;

import study.implementSpringIocByAnnotation.annotation.IocService;
import study.implementSpringIocByAnnotation.service.OrderService;

//@IocService(name = "orderService")
@IocService
public class OrderServiceImpl implements OrderService {

    @Override
    public String order(String username) {
        return "用户的名称为：" + username + "编号为：0001";
    }
}
