package study.implementSpringIocByAnnotation.service.impl;

import study.implementSpringIocByAnnotation.annotation.IocResource;
import study.implementSpringIocByAnnotation.annotation.IocService;
import study.implementSpringIocByAnnotation.service.OrderService;
import study.implementSpringIocByAnnotation.service.UserService;

@IocService(name = "userbiz")
public class UserServiceImpl implements UserService {

    @IocResource
    private OrderService orderServiceImpl; // 没有个service添加注解名称，就以小写首字母开头的内容为主。

    @Override
    public String findOrder(String username) {
        return orderServiceImpl.order(username);
    }
}
