package study.implementSpringIocByAnnotation.service;

public interface OrderService {
    String order(String username);
}
