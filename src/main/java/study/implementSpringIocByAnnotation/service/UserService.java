package study.implementSpringIocByAnnotation.service;

public interface UserService {
    String findOrder(String username);
}
