package study.implementSpringIocByAnnotation.context;

import study.implementSpringIocByAnnotation.annotation.IocResource;
import study.implementSpringIocByAnnotation.annotation.IocService;
import study.implementSpringIocByAnnotation.util.ClassUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class SpringContext {
    private String path;
    ConcurrentHashMap<String, Object> initBean = null;

    public SpringContext(String path) {
        this.path = path;
    }

    /*
     * 根据beanId获取对应的bean
     * */
    public Object getBean(String beanId) throws Exception {
        List<Class> classes = findAnnotataionService();
        if (classes == null || classes.isEmpty()) {
            throw new Exception("no found anything bean id ");
        }
        initBean = initBean(classes);
        if (initBean == null || initBean.isEmpty()) {
            throw new Exception("initial bean is empty or null");
        }
        Object object = initBean.get(beanId);
        // 初始化属性的依赖
        initAttribute(object);
        return object;
    }

    // 初始化属性的依赖
    private void initAttribute(Object object) throws Exception {
        // 获取object的所有的类型
        Class<? extends Object> classinfo = object.getClass();
        // 获取所有的属性字段。
        Field[] fields = classinfo.getDeclaredFields();

        // 遍历所有的字段
        for (Field field : fields) {
            // 查找字段上有依赖的注解
            boolean flag = field.isAnnotationPresent(IocResource.class);
            if (flag) {
                IocResource iocResource = field.getAnnotation(IocResource.class);
                if (iocResource != null) {
                    // 获取属性的beanId
                    String beandId = field.getName();
                    // 获取对应的object
                    Object attrObject = getBean(beandId);
                    if (attrObject != null) {
                        // 访问私有的字段
                        field.setAccessible(true);
                        // 赋值
                        field.set(object, attrObject);
                        continue;
                    }
                }
            }

        }
    }

    // 初始化bean
    public static ConcurrentHashMap<String, Object> initBean(List<Class> classes) throws
            IllegalAccessException, InstantiationException {
        ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();
        String beanId = "";
        for (Class clazz : classes) {
            Object object = clazz.newInstance();
            IocService annotation = (IocService) clazz.getDeclaredAnnotation(IocService.class);
            if (annotation != null) {
                // 如果定义了name属性，以实现的name属性为主的否则以默认的规则为主
                String value = annotation.name();
                if (value != null && !value.equals("")) {
                    beanId = value;
                } else {
                    beanId = toLowerCaseFirstOne(clazz.getSimpleName());
                }
            }
            map.put(beanId, object);
        }
        return map;
    }

    //查找包路径下的所有添加注解的类@IocService
    private List<Class> findAnnotataionService() throws Exception {
        if (path == null && path.equals("")) {
            throw new Exception("scan package address is null or empty");
        }
        // 获取报下面的所有的类i
        List<Class<?>> classes = ClassUtil.getClasses(path);
        if (classes == null || classes.size() == 0) {
            throw new Exception("not found service is added");
        }

        List<Class> annotationClasses = new ArrayList<>();
        for (Class clazz : classes) {
            //         通过反射机制，查找增加了注解的类
            IocService iocService = (IocService) clazz.getDeclaredAnnotation(IocService.class);
            if (iocService != null) {
                annotationClasses.add(clazz);
                continue;
            }
        }
        return annotationClasses;
    }

    // 首字母转换为小写
    public static String toLowerCaseFirstOne(String name) {
        if (Character.isLowerCase(name.charAt(0))) {
            return name;
        } else {
            return (new StringBuilder().append(Character.toLowerCase(name.charAt(0))).append(name.substring(1))).toString();
        }
    }

}
