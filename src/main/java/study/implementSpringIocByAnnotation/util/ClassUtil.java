package study.implementSpringIocByAnnotation.util;


/*
获取某个包下面的所有类信息
    */

import java.io.File;
import java.io.FileFilter;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassUtil {
    public static List<Class> getAllClassByInterface(Class c) {
        List<Class> retclassLsit = null;
        // jiekou
        if (c.isInterface()) {
            String packageNmae = c.getPackage().getName();
            List<Class<?>> allClass = getClasses(packageNmae);
            if (allClass != null) {
                retclassLsit = new ArrayList<>();
                for (Class classes : allClass) {
                    // 判断是否为同一个接口
                    if (c.isAssignableFrom(classes)) {
                        // 本身不加进去
                        if (!c.equals(classes)) {
                            retclassLsit.add(classes);
                        }
                    }
                }
            }
        }
        return retclassLsit;

    }

    // 获取某一类包所在的所有的类名，不包含迭代。
    public static String[] getPackageAllClassName(String classLocation, String packageName) {
//         将packageName分隔
        String[] packagePathSplit = packageName.split("[.]");
        String realclassLocation = classLocation;
//        int packgeLength = packagePathSplit.length;
        for (String str : packagePathSplit) {
            realclassLocation = realclassLocation + File.separator + str;
        }
        File packageDir = new File(realclassLocation);
        if (packageDir.isDirectory()) {
            String[] allclasses = packageDir.list();
            return allclasses;
        }
        return null;
    }

    // 从包中获取所有的class
    //
    public static List<Class<?>> getClasses(String packageName) {
        List<Class<?>> classList = new ArrayList<>();

        // 是否循环迭代
        boolean recursive = false;
        String packageDirName = packageName.replace('.', '/');
        Enumeration<URL> dirs;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
            // 循环迭代下去。
            while (dirs.hasMoreElements()) {
                // 获取下一个元素
                URL url = dirs.nextElement();
                // 得到协议的名称
                String protocal = url.getProtocol();
//                 如果是以文件的形式保存在服务器中
                if ("file".equals(protocal)) {
                    // 获取包的物理路径
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
//                    以文件的形式扫描整个包下的文件，并添加到集合中
                    findAndAddClassesInPackageByFile(packageName, filePath, recursive, classList);
                } else if ("jar".equals(protocal)) {
//                    以jar包文件形式
                    JarFile jarFile;
                    try {
//                        获取jar包，
                        jarFile = ((JarURLConnection) url.openConnection()).getJarFile();
//                        从此jar包得到一个枚举类
                        Enumeration<JarEntry> entries = jarFile.entries();
                        while (entries.hasMoreElements()) {
                            // 获取jar里面的每一个实体，可以是目录或者是文件jar包中的文件，
                            JarEntry jarEntry = entries.nextElement();
                            String name = jarEntry.getName();
//                            如果是以/开头的
                            if (name.charAt(0) == '/') {
                                name = name.substring(1);
                            }
//                            如果是前半部分和定义的包名相同
                            if (name.startsWith(packageDirName)) {
                                int idx = name.lastIndexOf('/');
                                if (idx != -1) {
                                    packageName = name.substring(0, idx).replace('/', '.');
                                }
                                if ((idx != -1) || recursive) {
//                                  如果可以迭代下去，并且是一个包
                                    if (name.endsWith(".class") && !jarEntry.isDirectory()) {
//                                  去掉后面的“.class”获取真正的类名，
                                        String className = name.substring(packageName.length() + 1
                                                , name.length() - 6);
                                        try {
                                            // 添加到classes
                                            classList.add(Class.forName(packageName + "." + className));
                                        } catch (ClassNotFoundException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return classList;
    }


    public static void findAndAddClassesInPackageByFile(String packageName, String packagePath, final boolean recursive,
                                                        List<Class<?>> classList) {
        // 获取此包的目录，建立一个File
        File dir = new File(packagePath);
        // 如果不存在或者，也不是目录就直接返回
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        // 如果存在 就获取包下的所有文件，包括目录
        File[] dirfiles = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        //       循环所有的文件
        for (File file : dirfiles) {
            // 目录继续扫描
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + "." + file.getName()
                        , file.getAbsolutePath(), recursive, classList);
            } else {
                // 如果没有java类文件，去掉后面的.class只留下类。
                String className = file.getName().substring(0, file.getName().length() - 6);
                try {
                    classList.add(Class.forName(packageName + "." + className));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

        }


    }
}
