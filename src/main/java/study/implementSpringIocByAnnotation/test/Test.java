package study.implementSpringIocByAnnotation.test;

import study.implementSpringIocByAnnotation.context.SpringContext;
import study.implementSpringIocByAnnotation.service.UserService;

public class Test {
    public static void main(String[] args) throws Exception {
//                    "study.implementSpringIocByAnnotation.service.impl"
//        需要扫描的包
        String path = "study.implementSpringIocByAnnotation.service.impl";
//        容器装载其中的bean，所有的bean都会存放其中。对应bean的名称获取需要注意问题。
        SpringContext springContext = new SpringContext(path);

        UserService userService = (UserService) springContext.getBean("userbiz");
        System.out.println(userService.findOrder("my name: pj"));
    }
}
