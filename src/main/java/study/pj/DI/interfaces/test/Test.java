package study.pj.DI.interfaces.test;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import study.pj.IOC.config.Appconfig;
import study.pj.DI.interfaces.Person;
import study.pj.DI.interfaces.impl.BussPerson;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Appconfig.class);
        Person p = ctx.getBean(BussPerson.class);
        p.service();
        ctx.close();

    }
}
