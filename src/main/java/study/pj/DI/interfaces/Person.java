package study.pj.DI.interfaces;

public interface Person {
     void service();

     void setAnimal(Animal animal);
}
