package study.pj.DI.interfaces.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import study.pj.DI.interfaces.Animal;
import study.pj.DI.interfaces.Person;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class BussPerson implements Person, BeanNameAware, BeanFactoryAware, ApplicationContextAware
        , InitializingBean, DisposableBean {
    //
//    @Autowired
//    @Qualifier("dog")
    private Animal animal;

    public void service() {
        System.out.println(this.animal.hashCode());
        this.animal.use();
    }

    @Autowired
    @Qualifier("dog")
    public void setAnimal(Animal animal) {
        System.out.println("实现延迟依赖注入");
        this.animal = animal;
    }

    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("【" + this.getClass().getSimpleName() + "】调用BeanFactoryAware的setBeanFactory:" + beanFactory);
    }

    public void setBeanName(String beanName) {
        System.out.println("【" + this.getClass().getSimpleName() + "】调用BeanNameAware的setBeanName:" + beanName);

    }


    public void destroy() throws Exception {
        System.out.println("【" + this.getClass().getSimpleName() + "】DisposableBean的方法");
    }

    public void afterPropertiesSet() throws Exception {
        System.out.println("【" + this.getClass().getSimpleName() + "】调用BeanNameAware的setBeanName");
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("【" + this.getClass().getSimpleName() + "】调用ApplicationAware的setApplicationContext:" + applicationContext);
    }

    @PreDestroy
    public void destroy1() throws Exception {
        System.out.println("【" + this.getClass().getSimpleName() + "】注解@PostDestroy定义的自定义销毁方法");
    }

    @PostConstruct
    public void init() {
        System.out.println("【" + this.getClass().getSimpleName() + "】注解@PostConstruct定义的自定义初始化方法！");
    }
}
