package study.pj.DI.interfaces.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import study.pj.DI.interfaces.Animal;

@Component
@Primary
public class Cat implements Animal {
    public void use() {
        System.out.println("猫抓老鼠：");
    }
}
