package study.pj.DI.interfaces.impl;

import org.springframework.stereotype.Component;
import study.pj.DI.interfaces.Animal;

@Component
public class Dog implements Animal {

    public void use() {
     System.out.println("狗看门");
    }
}
