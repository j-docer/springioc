package study.pj.IOC.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import study.pj.IOC.pojo.User;
import study.pj.IOC.config.Appconfig;

public class IocTest {
    public static void main(String[] args) {
        // 从IOC容器中获取的bean，一般为单例的BEAN
        ApplicationContext ctx = new AnnotationConfigApplicationContext(Appconfig.class);
        User user = ctx.getBean(User.class);
        System.out.println("user:" + user.getId() + "-" + user.getName() + "-" + user.getNote());
    }
}
