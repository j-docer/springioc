package study.pj.IOC.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;

@Data
@ComponentScan(basePackages = "study.com.IOC.pojo.*")
public class User {
    @Value("1")
    private int id;
    @Value("pj")
    private String name;
    @Value("IOC Test")
    private String note;
}
