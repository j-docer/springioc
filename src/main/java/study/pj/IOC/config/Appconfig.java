package study.pj.IOC.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "study.*", lazyInit = true)
public class Appconfig {
//    @Bean(name = "user")
//    public User initUser() {
//        User user = new User();
////        user.setId(1);
////        user.setName("pj");
////        user.setNote("Test IOC");
//        return user;
//    }
}
