package study.basic.set;

import java.util.ArrayList;
import java.util.List;

public class ArrayListTest {
    //    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("Test");
        list.add("Hello World");

        List<String> bList = new ArrayList<>(list);
        System.out.println(bList.toArray()==new Object[0]);

    }

}
