package study.basic.set;

import java.util.Arrays;
import java.util.List;

public class ToArray {
    public static void main(String[] args) {
        List l = Arrays.asList(args);
        System.out.println(l.toArray());
        System.out.println(l.toArray(new Object[0]));
    }
}
